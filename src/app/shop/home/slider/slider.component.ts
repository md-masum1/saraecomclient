import { Component, OnInit, Input } from '@angular/core';
import { Slider } from '../model/slider';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @Input() sliders: Slider[];
  fileUrl = environment.fileUrl;
  // Slick slider config
  public sliderConfig: any = {
    autoplay: true,
    autoplaySpeed: 3000
  };

  constructor() { }

  ngOnInit() {}

   getUrl(slider: Slider) {
     const url = this.fileUrl +  slider.imagePath;
    const path = url.replace(/\\/g, '/');
    return 'url(' +  path + ')';
   }

}
