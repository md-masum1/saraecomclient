import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HomeService } from 'src/app/shared/services/home.service';
import { AlertifyService } from 'src/app/shared/services/alertify.service';
import { Banner } from '../model/banner';

@Injectable()
export class BannerListResolver implements Resolve<Banner[]> {

    constructor(private homeService: HomeService, private router: Router, private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Banner[]> {
        return this.homeService.getBanner().pipe(
            catchError(error => {
                this.alertify.error('Problem retriving the Banner');
                this.router.navigate(['/home/ecom']);
                return of(null);
            })
        );
    }
}
