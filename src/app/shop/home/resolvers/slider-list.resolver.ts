import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Slider } from '../model/slider';
import { HomeService } from 'src/app/shared/services/home.service';
import { AlertifyService } from 'src/app/shared/services/alertify.service';

@Injectable()
export class SliderListResolver implements Resolve<Slider[]> {

    constructor(private homeService: HomeService, private router: Router, private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Slider[]> {
        return this.homeService.getSlider().pipe(
            catchError(error => {
                this.alertify.error('Problem retriving the Slider');
                this.router.navigate(['/home/ecom']);
                return of(null);
            })
        );
    }
}
