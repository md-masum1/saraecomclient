import { Component, OnInit, Input } from '@angular/core';
import { Banner } from '../model/banner';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-collection-banner',
  templateUrl: './collection-banner.component.html',
  styleUrls: ['./collection-banner.component.scss']
})
export class CollectionBannerComponent implements OnInit {
  @Input() banners: Banner[];
  fileUrl = environment.fileUrl;

  constructor() { }

  ngOnInit() {}

}
