export interface Slider {
    id: number;
    title: string;
    description: string;
    navigateUrl: string;
    displayOrder: string;
    imagePath: string;
  }
