import { Component, OnInit } from '@angular/core';
import { Product } from '../../shared/classes/product';
import { ProductsService } from '../../shared/services/products.service';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Slider } from './model/slider';
import { Banner } from './model/banner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public products: Product[] = [];
  sliders: Slider[];
  banners: Banner[];

  constructor(private productsService: ProductsService, private routeResolver: ActivatedRoute) {}

  ngOnInit() {
    this.routeResolver.data.subscribe(data => {
      this.sliders = data['sliders'];
      this.banners = data['banners'];
    });
    this.productsService.getProducts().subscribe(product => this.products = product);
  }

}
