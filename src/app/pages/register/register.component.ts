import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private toastrService: ToastrService,
              private location: Location) { }

  ngOnInit() {
    if (this.authService.loggedIn()) {
      this.location.back();
    }
    this.createRegisterForm();
  }

  createRegisterForm() {
    this.registerForm = this.fb.group({
      userName: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(): void {
    this.authService.register(this.registerForm.value).subscribe(() => {
      this.toastrService.success('registration successfully');
      this.location.back();
    }, error => {
    console.log(error);
    this.toastrService.error('Failed to register');
    });
  }

  onClear(): void {
    this.registerForm.reset();
  }

}
