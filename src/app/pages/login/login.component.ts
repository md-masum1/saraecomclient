import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  model: any = {};

  constructor(private authService: AuthService,
              private router: Router,
              private toastrService: ToastrService,
              private location: Location) { }

  ngOnInit() {
    if (this.authService.loggedIn()) {
      this.router.navigate(['/home/ecom']);
    }
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.toastrService.success('login successfull');
    }, error => {
      this.toastrService.error('Failed to login');
    }, () => {
      this.location.back();
    });
  }

}
