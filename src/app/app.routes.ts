import { Routes } from '@angular/router';

import { MainComponent } from './main/main.component';
import { MenuListResolver } from './shared/resolvers/menu-list.resolver';

export const rootRouterConfig: Routes = [
  {
    path: '',
    redirectTo: 'home/ecom',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MainComponent,
    resolve: {
      menus: MenuListResolver
    },
    children: [
      {
        path: 'home',
        loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule)
      },
      {
        path: 'pages',
        loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
      },
      {
        path: 'blog',
        loadChildren: () => import('./blog/blog.module').then(m => m.BlogModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'pages/404'
  }
];

