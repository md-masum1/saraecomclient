import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { ProductsService } from '../shared/services/products.service';
import { CartService } from '../shared/services/cart.service';
import { WishlistService } from '../shared/services/wishlist.service';
import { Menu } from '../shared/header/widgets/navbar/navbar-items';
declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [ProductsService, CartService, WishlistService]
})
export class MainComponent implements OnInit {
  menus: Menu[];
  public url: any;

  constructor(private router: Router, private routeResolver: ActivatedRoute) {
    this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.url = event.url;
          if (this.url === '/home') {
            this.router.navigate(['home/ecom']);
          }
        }
    });
  }

  ngOnInit() {
    this.routeResolver.data.subscribe(data => {
      this.menus = data['menus'];
    });
   $.getScript('assets/js/script.js');
  }

}
