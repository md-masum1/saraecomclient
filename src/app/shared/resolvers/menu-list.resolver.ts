import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from '../services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Menu } from '../header/widgets/navbar/navbar-items';
import { HomeService } from '../services/home.service';

@Injectable()
export class MenuListResolver implements Resolve<Menu[]> {

    constructor(private homeService: HomeService, private router: Router, private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Menu[]> {
        return this.homeService.getMenu().pipe(
            catchError(error => {
                this.alertify.error('Problem retriving the menu');
                this.router.navigate(['/home/ecom']);
                return of(null);
            })
        );
    }
}
