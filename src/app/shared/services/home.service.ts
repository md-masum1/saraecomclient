import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  baseUrl = environment.apiUrl + 'ecom/';

  constructor(private http: HttpClient) { }

  getMenu() {
    return this.http.get(this.baseUrl + 'home/menus');
  }

  getSlider() {
    return this.http.get(this.baseUrl + 'home/sliders');
  }

  getBanner() {
    return this.http.get(this.baseUrl + 'home/banners');
  }

}
