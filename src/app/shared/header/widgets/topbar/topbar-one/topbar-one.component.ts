import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../../../shared/services/products.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar-one.component.html',
  styleUrls: ['./topbar-one.component.scss']
})
export class TopbarOneComponent implements OnInit {

  constructor(public productsService: ProductsService,
              public authService: AuthService,
              private router: Router,
              private toastrService: ToastrService) { }

  ngOnInit() {
  }

  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.toastrService.info('loged out');
    this.router.navigate(['/home/ecom']);
  }

}
