import { Component, OnInit, Input } from '@angular/core';
import { Menu, MENUITEMS } from './navbar-items';
import { HomeService } from 'src/app/shared/services/home.service';
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() menus: Menu[];
  public menuItems: Menu[];
  public toggleNavBar = false;

  constructor() { }

  ngOnInit() {
    this.menuItems = MENUITEMS.filter(menuItem => menuItem);
    // this.menuItems = this.menus;
  }

  toggleNav() {
    this.toggleNavBar = !this.toggleNavBar;
  }

}
