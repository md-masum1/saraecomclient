import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

   jwtHelper = new JwtHelperService();

   constructor(translate: TranslateService, private authService: AuthService) {
      translate.setDefaultLang('en');
      translate.addLangs(['en', 'fr']);
   }

   ngOnInit() {
      const token = localStorage.getItem('token');
      if (token) {
        this.authService.decodedToken = this.jwtHelper.decodeToken(token);
        console.log(this.authService.decodedToken);
      }
    }

    loggedIn() {
      return this.authService.loggedIn();
    }
}
